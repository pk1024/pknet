FROM	pk1024/pkconda:latest
MAINTAINER	Donald Raikes <dr1861@nyu.edu>

## Make sure base image is up-to-date:
USER	root
RUN	apt-get update && \
	apt-get upgrade -y && \
	conda upgrade --all

## Add new apt and pip packages:
RUN	apt-get install -y dnsutils nmap tcpdump sslsplit \
		tcpick  \
		tcpflow  \
		tcpreen  \
		tcpreplay  \
		tcpslice  \
		tcpspy \
		tcpstat \
		tcptrace  \
		tcptraceroute \
		tcptrack \
		tcputils \
		tcpwatch-httpproxy \
		tcpxtract  \
		traceroute 
RUN	pip install scapy scrapy scrapyelasticsearch  \
		httpcap \
		httpreplay \
		libpcap  \
		PacketReader \
		packetseq \
		pcap-parser  \
		python-pcapng  \
		pcapng  \
		pcappy 
